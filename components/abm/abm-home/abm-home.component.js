angular
    .module('abmHome')
    .component('abmHome', {
        templateUrl: 'components/abm/abm-home/abm-home.template.html',
        controller: ['$rootScope', 'Company', function AbmHomeController($rootScope, Company) {
            let self = this;

            self.contracts = [];
            self.company = $rootScope.user_info.company;

            //TODO: Pasar una clase que maneje las rutas de la aplicacion (environment class)
            self.links = [
                {
                    title: 'Home',
                    url: 'http://local.contracts.test/#!/abm/home'
                },
                {
                    title: 'Create Company',
                    url: 'http://local.contracts.test/#!/company/form'
                },
                {
                    title: 'Create Contract',
                    url: 'http://local.contracts.test/#!/contract/form'
                },
                {
                    title: 'Contract Procedures',
                    url: '#'
                }
            ];

            //TODO: contract edition, contract view, contract delete.

            self.onInitPage = function () {
                self.companyContracts();
                self.hasContracts();
            };

            self.companyContracts = function () {
                Company
                    .getCompanyById({id: $rootScope.user_info.company_id})
                    .$promise
                    .then(function (response) {
                        self.contracts = response.data.contracts;
                    }, function (error) {
                        window.console.log('There was an error.');
                    });
            };

            self.hasContracts = function () {
                return self.contracts.length > 0;
            };
        }]
    });
