angular
    .module('companyList')
    .component('companyList', {
        templateUrl: 'components/company/company-list/company-list.template.html',
        controller: ['Company', function CompanyListController(Company) {
            let self = this;

            self.companies = [];

            Company.
            getCompanies().
            $promise.
            then(function (response) {
                self.companies = response.data;
            }, function () {
                window.console.log('Ocurrió un error.');
            });
        }]
    });
