angular
    .module('companyDetail')
    .component('companyDetail', {
        templateUrl: 'components/company/company-detail/company-detail.template.html',
        controller: ['$routeParams', 'Company', function CompanyDetailController($routeParams, Company) {
            let self = this;

            self.company = {
                contracts: []
            };

            Company.
            getCompanyById({
                id: $routeParams.id
            }).
            $promise.
            then(function (response) {
                self.company = response.data;
            }, function () {
                window.console.log('Ocurrió un error.');
            });

            self.hasContracts = function () {
                return self.company.contracts.length > 0;
            };
        }]
    });
