angular
    .module('companyForm')
    .component('companyForm', {
        templateUrl: 'components/company/company-form/company-form.template.html',
        controller: ['$scope', 'Company', 'Upload', function CompanyFormController($scope, Company, Upload) {
            let self = this;

            self.company = {};

            self.save = function() {
                Company.
                    createCompany(Upload, self.company, function(response) {
                        window.console.log(response.data);
                    }, function (error) {
                        window.console.log(error);
                    });
            };
        }]
    });
