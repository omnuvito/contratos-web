angular
    .module('authLogin')
    .component('authLogin', {
        templateUrl: 'components/auth/auth-login/auth-login.template.html',
        controller: ['$location', '$rootScope', 'Auth', function AuthLoginController(
            $location,
            $rootScope,
            Auth
        ) {
            let self = this;

            self.user = {};

            self.login = function() {
                Auth
                    .login(self.user)
                    .$promise
                    .then(function(response) {
                        $rootScope.user_token = response.data.token;
                        $rootScope.user_info = response.data.user;

                        window.console.log('Log in successfully.');

                        $location.path("/abm/home/");
                    }, function (error) {
                        window.console.log('There was an error.')
                    });
            };

            self.logout = function() {
                delete $rootScope.user_token;
                delete $rootScope.user_info;

                window.console.log('Log in successfully.');

                $location.path("/abm/home/");
            };
        }]
    });
