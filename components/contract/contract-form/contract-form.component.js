angular
    .module('contractForm')
    .component('contractForm', {
        templateUrl: 'components/contract/contract-form/contract-form.template.html',
        controller: ['$location', '$rootScope', 'Contract', 'Upload', function ContractFormController($location, $rootScope, Contract, Upload) {
            let self = this;

            self.contract = {};

            self.save = function() {
                let data = {
                    company_id: $rootScope.user_info.company.id,
                    user_id: $rootScope.user_info.id,
                    title: self.contract.title,
                    description: self.contract.description,
                    file: self.contract.file
                };

                Contract.
                    createContract(Upload, data, function() {
                        $location.path("/abm/home/");
                    }, function (error) {
                        window.console.log(error);
                    });
            };
        }]
    });
