angular
    .module('contractProcedure')
    .component('contractProcedure', {
        templateUrl: 'components/procedure/contract-procedure/contract-procedure.template.html',
        controller: ['$scope', '$routeParams', 'ContractProcedure', function ContractProcedureController($scope, $routeParams, ContractProcedure) {
            let self = this;

            self.contractProcedure = {};

            ContractProcedure
                .getContractProcedureById({
                    id: $routeParams.id
                })
                .$promise
                .then(function (response) {
                    self.contractProcedure = response.data;

                    window.console.log(self.contractProcedure);
                    }, function () {
                    window.console.log('Ocurrió un error.');
                });
        }]
    });
