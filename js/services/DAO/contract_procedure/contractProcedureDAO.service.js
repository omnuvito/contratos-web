angular.
module('contractProcedureDAO').
factory('ContractProcedure', ['$resource',
    function ($resource) {
        let fact;

        fact = $resource('http://api.contracts.test/api/contract/procedures', {}, {
            getCompanies: {
                method: 'GET',
                url: 'http://api.contracts.test/api/contract/procedures'
            },
            getContractProcedureById: {
                method: 'GET',
                params: {id: '@id'},
                url: 'http://api.contracts.test/api/contract/procedures/:id'
            }
        });

        return fact;
    }
]);
