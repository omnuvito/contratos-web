angular.
module('companyDAO').
factory('Company', ['$resource',
    function ($resource) {
        let fact;
        fact = $resource('http://api.contracts.test/api/companies', {}, {
            getCompanies: {
                method: 'GET',
                url: 'http://api.contracts.test/api/companies'
            },
            getCompanyById: {
                method: 'GET',
                params: {id: '@id'},
                url: 'http://api.contracts.test/api/companies/:id'
            }
        });

        fact.createCompany = function(Upload, data, callback, errorCallback) {
            Upload
                .upload({
                    url: 'http://api.contracts.test/api/companies',
                    data: data,
                    timeout: 1800000
                })
                .then(function (response) {
                    window.console.log('Company upload successful.');
                    callback(response.data);
                }, function (error) { //catch error
                    if (errorCallback){
                        errorCallback();
                    }
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);
                    window.console.log('progress: ' + progressPercentage + '% ');
                });
        };

        return fact;
    }
]);
