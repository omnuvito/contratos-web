angular.
module('contractDAO').
factory('Contract', ['$resource',
    function ($resource) {
        let fact;

        fact = $resource('http://api.contracts.test/api/contracts', {}, {
            getCompanies: {
                method: 'GET',
                url: 'http://api.contracts.test/api/contracts'
            },
            getContractById: {
                method: 'GET',
                params: {id: '@id'},
                url: 'http://api.contracts.test/api/contracts/:id'
            },
            getContractsByCompanyId: {
                method: 'GET',
                params: {companyId: '@company_id'},
                url: 'http://api.contracts.test/api/contracts/company/:company_id'
            }
        });

        fact.createContract = function(Upload, data, callback, errorCallback) {
            Upload
                .upload({
                    url: 'http://api.contracts.test/api/contracts',
                    data: data,
                    timeout: 1800000
                })
                .then(function (response) {
                    window.console.log('Contract upload successful.');
                    callback(response.data);
                }, function (error) { //catch error
                    if (errorCallback){
                        errorCallback();
                    }
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);
                    window.console.log('progress: ' + progressPercentage + '% ');
                });
        };

        return fact;
    }
]);
