angular.
module('authDAO').
factory('Auth', ['$resource',
    function ($resource) {
        return $resource('http://api.contracts.test/api/auth/login', {}, {
            login: {
                method: 'POST',
                url: 'http://api.contracts.test/api/auth/login'
            }
        });
    }
]);
