angular.
    module('contractApp').
    config(['$locationProvider', '$routeProvider',
    function config ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
            when('/companies', {
                template: '<company-list></company-list>'
            }).
            when('/companies/:id', {
                template: '<company-detail></company-detail>'
            }).
            when('/contract/procedure/:id', {
                template: '<contract-procedure></contract-procedure>'
            }).
            when('/company/form', {
                template: '<company-form></company-form>'
            }).
            when('/contract/form', {
                template: '<contract-form></contract-form>'
            }).
            when('/auth/login', {
                template: '<auth-login></auth-login>'
            }).
            when('/abm/home', {
                template: '<abm-home></abm-home>'
            }).
            otherwise('/auth/login');
        }
    ]);
