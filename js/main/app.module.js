angular.module('contractApp', [
    'ngRoute',
    'ngResource',
    'authLogin',
    'abmHome',
    'companyList',
    'companyDetail',
    'contractList',
    'contractDetail',
    'companyForm',
    'contractForm',
    'contractProcedure',
    'api',
    'ngFileUpload'
]);
